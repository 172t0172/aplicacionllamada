package com.example.aplicacionllamada

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val comprobar_permiso = ContextCompat.checkSelfPermission(
                this, android.Manifest.permission.CALL_PHONE)
        if(comprobar_permiso != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf<String>(android.Manifest.permission.CALL_PHONE), 255)
        }
        imageButton.setOnClickListener{
            val intent =Intent(Intent.ACTION_CALL, Uri.parse("tel: 2791000023"))
            startActivity(intent)

        }
    }
}